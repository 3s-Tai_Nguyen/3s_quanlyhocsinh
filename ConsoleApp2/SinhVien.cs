﻿using System;

namespace ConsoleApp2
{
    public class SinhVien 
    {
        private string hoTen;
        private string maSV;
        private int diemToan;
        private int diemLy;
        private int diemHoa;

        public string HoTen
        {
            get => hoTen;
            set => hoTen = value;
        }

        public string MaSV
        {
            get => maSV;
            set => maSV = value;
        }

        public int DiemToan
        {
            get => diemToan;
            set => diemToan = value;
        }

        public int DiemLy
        {
            get => diemLy;
            set => diemLy = value;
        }

        public int DiemHoa
        {
            get => diemHoa;
            set => diemHoa = value;
        }

        public void NhapTT()
        {
            Console.Write("Nhap Ten: ");
            hoTen = Console.ReadLine();
            Console.Write("Nhap MSV: ");
            maSV = Console.ReadLine();
            Console.Write("Nhap diem Toan: ");
            do
            {
                diemToan = Convert.ToInt32(Console.ReadLine());
                if (diemToan > 10 || diemToan < 0)
                {
                    Console.WriteLine("Error! 0 < Diem Toan < 10");
                    Console.Write("Nhap lai Diem Toan:");
                }
                else break;
            } while (diemToan < 10 || diemToan > 0);

            Console.Write("Nhap diem Ly : ");
            do
            {
                diemLy = Convert.ToInt32(Console.ReadLine());
                if (diemLy > 10 || diemLy < 0)
                {
                    Console.WriteLine("Error! 0 < Diem Ly < 10");
                    Console.Write("Nhap lai Diem Ly:");
                }
                else break;
            } while (diemLy < 10 || diemLy > 0);

            Console.Write("Nhap diem Hoa : ");
            do
            {
                diemHoa = Convert.ToInt32(Console.ReadLine());
                if (diemHoa > 10 || diemHoa < 0)
                {
                    Console.WriteLine("Error! 0 < Diem Hoa < 10");
                    Console.Write("Nhap lai Diem Hoa:");
                }
                else break;
            } while (diemHoa < 10 || diemHoa > 0);

        }

        public void XuatTT()
        {
            Console.WriteLine("Ho Ten: {0}|| MSV: {1}|| DiemToan: {2}|| DiemLy|| {3}|| DiemHoa {4}|| DiemTB {5}", hoTen,
                maSV, diemToan, diemLy, diemHoa, DiemTB(diemToan, diemLy, diemHoa));
        }

        public float DiemTB(int diemtoan, int diemly, int diemhoa)
        {
            return (float) ((diemtoan + diemly + diemhoa) / 3.0);
        }
    }
}